# Create environment

We finidhed last part with a refactoring and we will start this one by... refactoring! The changes are being made
to player movement system. Now `GameState` will handle this. The start is to define movement deltas associative
array. This has to be done at runtime (not compile-time), so we do it in the `GameState` initializer.

```(d)
// this is to show what movement delta is
alias Tuple!(int, int) MovementDelta;

this.movementDeltas = [
                      terminal.keycode.up :   MovementDelta(0, -1),
                      terminal.keycode.down : MovementDelta(0, 1),
                      terminal.keycode.left : MovementDelta(-1, 0),
                      terminal.keycode.right: MovementDelta(1, 0)
                      ];
```

How do we use it? In our `handleKey` function we group all arrows together and use a new `attemptToMovePlayer` function:

```(d)
case terminal.keycode.up:
case terminal.keycode.down:
case terminal.keycode.right:
case terminal.keycode.left:
  MovementDelta delta = movementDeltas[key];
  attemptToMovePlayer(delta);
  break;
```

```(d)
void attemptToMovePlayer(MovementDelta delta) {
  int new_x = this.player.x + delta[0];
  int new_y = this.player.y + delta[1];

  if(this.canPlayerMoveTo(new_x, new_y)) {
    player.x = new_x;
    player.y = new_y;
  }
}
```

Now instead of keeping user ini game after making a move, we check the conditions before actual move. The reason here is that we will
implement walls soon and we want to prevent player from going into them as well. Note that this means that if the player makes a move
which is illegal, the turn will still pass. This is ok for now, as most roguelikes do that too.

What's left in the picture is `canPlayerMoveTo` function. Now it only checks if we stay in the game area. As you can notice,
we also made `GameState` aware of terminal sizes. This makes sense, as these are our game boundaries too, not only UI detail.

```(d)
  bool canPlayerMoveTo(int x, int y) {
    if(x < 0 || x >= this.windowWidth || y < 0 || y >= this.windowHeight) return false;
    return true;
  }
```
