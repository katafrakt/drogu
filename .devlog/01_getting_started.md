# Getting started

This is a first entry in devlog for Drogu project. It's an attempt to write a roguelike game in D programming language
using pretty cool [BearLibTerminal](http://foo.wyrd.name/en:bearlibterminal) library. I'm no expert in D (although
it's not my first time with this language) so deep knowledge is not required to read it. I'm not going into any
details regarding the language itself though.

I based my knowledge on two cool existing tutorials:
* [Complete Roguelike Tutorial](http://www.roguebasin.com/index.php?title=Complete_Roguelike_Tutorial,_using_python%2Blibtcod) by RogueBasin, a classic, written for Python and libtcod
* [Roguelike Development](https://jeremyceri.se/roguelikes/) by Jeremy Cerise, targetting BearLibTerminal and Go

## Let's launch some window!

Without further ado, lets get started.

Assuming that you have D compiler and `dub` installed, create a new project: `dub init drogu`. When asked about dependencies, put `bearlibterminal` there.

Now let's add some code in `source/app.d`:

```(d)
import std.stdio;
import BearLibTerminal;

void main()
{
  terminal.open("Drogu - a roguelike in D");
  terminal.refresh();
  while(true) {
    bool should_exit = false;
    int key = terminal.read();

    switch(key) {
    case terminal.keycode.escape:
      should_exit = true;
      break;
    default:
      writeln("Pressed: ", key);
    }

    if(should_exit) break;
  }
  terminal.close();
}

```

The code is pretty straightforward. LibBearTerminal (and its wrapper in D) is not very nice - you have to use functions from global namespace `terminal`. This is unfortunate,
but we can live with that. Later, to perform some damage control, we'll wrap it in some additional structures, to keep leaking at bay.

But anyway, we open a new terminal, giving it a title. Then we start a loop. In there, there is a blocking `terminal.read()` invocation, which waits for some action from user - namely a key press.
After that, we check if pressed key was ESC. If so, we set `should_break` variable to true and effectively break the loop later. Otherwise let's just print a code of pressed key to the terminal.

With this, after running `dub run`, we'll see an empty black window. We can press some keys and observer the console output. Finally, after pressing ESC, the console will shut down and the
program will exit.

## Drawing a player

Even though it might seem rewarding, we didn't really come any closer to have something game-ish. To change that, let's draw the player! In roguelike games player is usually indicated by `@` sign and (for now) it will be no different here. Where do we want it? How about the center of the screen.

We will begin with creating a `Player` struct. It will only hold two values: position x and y of the player on the screen.

```(d)
struct Player {
  uint x;
  uint y;
}
```

To know correct starting position, we have to calculate it:

```(d)
uint terminal_width = terminal.state(terminal.keycode.width);
uint terminal_height = terminal.state(terminal.keycode.height);
```

BearLibTerminal expresses many things with keycodes and does so for state to. That's why this construct may seem weird (after all there was no key pressed, so why the key code?), but this 
is how it works.

After that it's easy to just create a player and draw it on the screen:

```(d)
auto player = Player(terminal_width/2, terminal_height/2);
terminal.print(player.x, player.y, "@");
```

And just like that, we have a player (marked with `@`) drawn in the center of the screen.

## I like to move it, move it!

So far the game is not very interactive, but that's about to change. Our character needs to move around and we are going to make him do so! As you can imagine,
we will first of all extend the `switch` statement:

```(d)
switch(key) {
case terminal.keycode.up:
  player.y--;
  break;
case terminal.keycode.down:
  player.y++;
  break;
case terminal.keycode.right:
  player.x++;
  break;
case terminal.keycode.left:
  player.x--;
  break;
case terminal.keycode.escape:
  should_exit = true;
  break;
default:
  writeln("Pressed: ", key);
}
```

This is simple: we need to check for all possible direction and appropriately change player's position. Note that position `0,0` is in the top-left corner of our game arena.

In order not to leave trail of `@`s behind us, we also need to clear previous player position, before drawing it in a new one:

```(d)
terminal.print(player.x, player.y, " ");
```

There is one more thing to do. When player wants to go out of game area, let's not let him. So after the movement but before drawing, let's perform a simple check if our player
does not land outside of the area.

```(d)
void keepPlayerInGameArea(int width, int height, ref Player player) {
  if(player.x < 0) player.x = 0;
  if(player.y < 0) player.y = 0;
  if(player.x >= width) player.x = width-1;
  if(player.y >= height) player.y = height-1;
}
```

Our whole game loop now looks like that:

```(d)
  while(true) {
    terminal.refresh();

    bool should_exit = false;
    int key = terminal.read();

    // clear previous position
    terminal.print(player.x, player.y, " ");

    switch(key) {
    case terminal.keycode.up:
      player.y--;
      break;
    case terminal.keycode.down:
      player.y++;
      break;
    case terminal.keycode.right:
      player.x++;
      break;
    case terminal.keycode.left:
      player.x--;
      break;
    case terminal.keycode.escape:
      should_exit = true;
      break;
    default:
      writeln("Pressed: ", key);
    }

    if(should_exit) break;

    keepPlayerInGameArea(terminal_width, terminal_height, player);
    terminal.print(player.x, player.y, "@");
  }
```

`dub run` and see that you are able to move around using arrows on the keyboard.

## Red, green, REFACTOR

Since we achieved something, it's a good time to take a step back and clean up some things. I decided to split current functionality to a couple of smaller files. This way
I can limit places where `terminal` namespace might be used but also I'm preparing for growing codebase which will surely come later. After those changes I have:
* `window.d` which wraps terminal-related things
* `player.d` - where `Player` strict lives; I also moved `keepPlayerInGameArea` there
* `game_state.d` - not much of a function now, but it will hold the state


`window.d`:
```(d)
import BearLibTerminal;
import game_state;

struct Window {
  uint width;
  uint height;

  this(string title) {
    terminal.open(title);
    this.width = terminal.state(terminal.keycode.width);
    this.height = terminal.state(terminal.keycode.height);
  }

  void drawState(GameState state) {
    clear();
    drawPlayer(state);
    refresh();
  }

  void drawPlayer(GameState state) {
    auto player = state.player;
    terminal.print(player.x, player.y, "@");
  }

  void refresh() {
    terminal.refresh();
  }

  void clear() {
    terminal.clear_area(0, 0, this.width, this.height);
  }

  void close() {
    terminal.close();
  }

  int readKey() {
    return terminal.read();
  }
}
```

`player.d`:
```(d)
import window;

struct Player {
  int x;
  int y;

  void keepInWindow(ref Window window) { // not perfect - player does not need to know about window
    if(this.x < 0) this.x = 0;
    if(this.y < 0) this.y = 0;
    if(this.x >= window.width) this.x = window.width-1;
    if(this.y >= window.height) this.y = window.height-1;
  }
}
```

`game_state.d` (handling keypress has been moved here, though it probably deserves separate place):
```(d)
import std.stdio;
import BearLibTerminal;
import player;

struct GameState {
  Player* player;

  this(ref Player player) {
    this.player = &player;
  }

  bool handleKey(uint key) {
    auto player = this.player;

    switch(key) {
    case terminal.keycode.up:
      player.y--;
      break;
    case terminal.keycode.down:
      player.y++;
      break;
    case terminal.keycode.right:
      player.x++;
      break;
    case terminal.keycode.left:
      player.x--;
      break;
    case terminal.keycode.escape:
      return true;
    default:
      writeln("Pressed: ", key);
    }

    return false;
  }
}
```

finally, `app.d`:
```(d)
import std.stdio;
import window;
import player;
import game_state;

void main()
{
  auto window = Window("Drogu - a roguelike in D");
  auto player = Player(window.width/2, window.height/2);
  auto state = GameState(player);
  bool should_exit = false;

  while(true) {
    player.keepInWindow(window);
    window.drawState(state);

    int key = window.readKey();
    should_exit = state.handleKey(key);
    if(should_exit) break;
  }

  window.close();
}
```
