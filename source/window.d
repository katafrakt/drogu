import BearLibTerminal;
import game_state;

struct Window {
  uint width;
  uint height;

  this(string title) {
    terminal.open(title);
    this.width = terminal.state(terminal.keycode.width);
    this.height = terminal.state(terminal.keycode.height);
  }

  void drawState(GameState state) {
    clear();
    drawPlayer(state);
    refresh();
  }

  void drawPlayer(GameState state) {
    auto player = state.player;
    terminal.print(player.x, player.y, "@");
  }

  void refresh() {
    terminal.refresh();
  }

  void clear() {
    terminal.clear_area(0, 0, this.width, this.height);
  }

  void close() {
    terminal.close();
  }

  int readKey() {
    return terminal.read();
  }
}
