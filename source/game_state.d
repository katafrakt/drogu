import std.stdio;
import std.typecons;
import BearLibTerminal;
import player;

struct GameState {
  alias Tuple!(int, int) MovementDelta;

  Player* player;
  int windowWidth;
  int windowHeight;
  MovementDelta[int] movementDeltas;

  this(ref Player player, int windowWidth, int windowHeight) {
    this.player = &player;
    this.windowWidth = windowWidth;
    this.windowHeight = windowHeight;

    // holds position deltas depending on the key pressed
    // must be done in runtime, otherwise throws non-constant expression error
    this.movementDeltas = [
                          terminal.keycode.up :   MovementDelta(0, -1),
                          terminal.keycode.down : MovementDelta(0, 1),
                          terminal.keycode.left : MovementDelta(-1, 0),
                          terminal.keycode.right: MovementDelta(1, 0)
                          ];
  }

  void attemptToMovePlayer(MovementDelta delta) {
    int new_x = this.player.x + delta[0];
    int new_y = this.player.y + delta[1];

    if(canPlayerMoveTo(new_x, new_y)) {
      player.x = new_x;
      player.y = new_y;
    }
  }

  bool canPlayerMoveTo(int x, int y) {
    // check if we are in game arena
    if(x < 0 || x >= this.windowWidth || y < 0 || y >= this.windowHeight) return false;

    return true;
  }

  bool handleKey(uint key) {
    auto player = this.player;

    switch(key) {
    case terminal.keycode.up:
    case terminal.keycode.down:
    case terminal.keycode.right:
    case terminal.keycode.left:
      MovementDelta delta = movementDeltas[key];
      attemptToMovePlayer(delta);
      break;
    case terminal.keycode.escape:
      return true;
    default:
      writeln("Pressed: ", key);
    }

    return false;
  }
}
