import std.stdio;
import window;
import player;
import game_state;

void main()
{
  auto window = Window("Drogu - a roguelike in D");
  auto player = Player(window.width/2, window.height/2);
  auto state = GameState(player, window.width, window.height);
  bool should_exit = false;

  while(true) {
    window.drawState(state);

    int key = window.readKey();
    should_exit = state.handleKey(key);
    if(should_exit) break;
  }

  window.close();
}
